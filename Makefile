##
## Makefile for abstractvm in /Users/jean/Documents/epitech/cpp_abstractvm
##
## Made by jean gravier
## Login   <gravie_j@epitech.eu>
##
## Started on  Mon Feb  17 18:43:38 2014 jean gravier
## Last update Tue Feb 25 16:57:48 2014 Fritsch harold
##

NAME			=	201yams

CC	 		=	g++

RM			=	rm -f

SRCS			=	main.cpp \
				Yams.cpp \

OBJS			=	$(SRCS:.cpp=.o)

CPPFLAGS		+=	-Wall -Wextra

$(NAME): $(OBJS)
	$(CC) $(SRCS) -o $(NAME)

all: $(NAME)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: clean fclean all