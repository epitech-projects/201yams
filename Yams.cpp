//
// Yams.cpp for  in /home/fritsc_h/projets/201yams
// 
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
// 
// Started on  Tue Feb 25 16:32:20 2014 Fritsch harold
// Last update Fri Feb 28 17:13:27 2014 Fritsch harold
//

#include <valarray>
#include <algorithm>
#include <iostream>
#include <ios>
#include <string>
#include <cstdlib>
#include <map>
#include "Yams.hh"

Yams::Yams(char **args)
{
  std::map<std::string, void (Yams::*)()>	func_ptr;
  std::string	str(args[6]);
  std::string	temp;
  size_t	n;

  _d[0] = atoi(args[1]);
  _d[1] = atoi(args[2]);
  _d[2] = atoi(args[3]);
  _d[3] = atoi(args[4]);
  _d[4] = atoi(args[5]);
  func_ptr["paire"] = &Yams::paire;
  func_ptr["brelan"] = &Yams::brelan;
  func_ptr["carre"] = &Yams::carre;
  func_ptr["yams"] = &Yams::yams;
  n = std::count(str.begin(), str.end(), '_');
  if (n == 1 || n == 2)
    {
      _comb = str.substr(0, str.find_first_of("_", 0));
      if (n == 1)
	{
	  temp = str.substr(str.find_first_of("_", 0) + 1, str.length() - 2);
	  _comb_arg1 = atoi(temp.data());
	  _comb_arg2 = 0;
	  if ((_comb == "paire") || (_comb == "brelan") || (_comb == "carre") || (_comb == "yams") || (_comb == "suite"))
	    (this->*func_ptr[_comb])();
	  else
	    std::cerr << "Wrong combinaison, usage : ./201yams de1 de2 de3 de4 de5 yams_1_2" << std::endl;
	}
      else if (n == 2)
	{
	  temp = str.substr(str.find_first_of("_", 0) + 1, str.find_last_of("_", 0) - str.find_first_of("_", 0));
	  _comb_arg1 = atoi(temp.data());
	  temp = str.substr(str.find_first_of("_", str.find_first_of("_", 0) + 1) + 1, 1);
	  _comb_arg2 = atoi(temp.data());
	  if (_comb == "pull")
	    ;
	  else
	    std::cerr << "Wrong combinaison, usage : ./201yams de1 de2 de3 de4 de5 yams_1_2" << std::endl;
	}
    }
  else
    std::cerr << "Wrong combinaison, usage : ./201yams de1 de2 de3 de4 de5 yams_1_2" << std::endl;
}

void		Yams::yams()
{
  size_t	i = 0;
  size_t	count = 0;
  float		percentage = 0;

  while	(i < 5)
    {
      if (_d[i] == _comb_arg1)
	++count;
      ++i;
    }
  percentage = (1.00 / (float)std::pow((6), 5 - count)) * 100.00;
  print(percentage, "yams");
}

void		Yams::carre()
{
  size_t	i = 0;
  size_t	count = 0;
  float		percentage = 0;

  while (i < 5)
    {
      if (_d[i] == _comb_arg1)
	++count;
      ++i;
    }
  percentage = ((((5.00 - (float)count) * 5.00) / (float)std::pow(6, 5 - count)) +
		(1.00 / (float)std::pow(6, 5 - count))) * 100.00;
  print(percentage, "carré");
}

void		Yams::brelan()
{
  size_t	i = 0;
  size_t	count = 0;
  float		percentage = 0;

  while (i < 5)
    {
      if (_d[i] == _comb_arg1)
	++count;
      ++i;
    }
  percentage = (((((5.00 - (float)count) * 40.00) / (float)std::pow(6, 5 - count)) +
		 (((5.00 - (float)count) * 5.00) / (float)std::pow(6, 5 - count)) +
		 (1.00 / (float)std::pow(6, 5 - count))) * 100);
  print(percentage, "brelan");
}

void		Yams::paire()
{
  size_t	i = 0;
  size_t	count = 0;
  float		percentage = 0;

  while (i < 5)
    {
      if (_d[i] == _comb_arg1)
	++count;
      ++i;
    }
  percentage = ((((5.00 - (float)count) * 40.00) / (float)std::pow(6, 5 - count)) + (((5.00 - (float)count) * 40.00) / (float)std::pow(6, 5 - count))
		+ (((5.00 - (float)count) * 5.00) / (float)std::pow(6, 5 - count)) + (1.00 / (float)std::pow(6, 5 - count))) * 100;
  print(percentage, "paire");
}

void		Yams::print(float percentage, std::string combi)
{
  std::cout.precision(2);
  std::cout << std::fixed;
  std::cout << "probabilité d’obtenir un " << combi << " de " << _comb_arg1 << " : ";
  if (percentage >= 100.00)
    std::cout << "100";
  else
    std::cout << percentage;
  std::cout << "%" << std::endl;
}

Yams::~Yams()
{
  ;
}
