//
// Yams.hh for  in /home/fritsc_h/projets/201yams
// 
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
// 
// Started on  Tue Feb 25 16:26:23 2014 Fritsch harold
// Last update Thu Feb 27 22:59:46 2014 Fritsch harold
//

#ifndef YAMS_HH_
# define YAMS_HH_
# include <string>

class		Yams
{
public:
  Yams(char **args);
  ~Yams();
  void		carre();
  void		yams();
  void		brelan();
  void		paire();
  void		print(float, std::string);

private:
  int		_d[5];
  std::string	_comb;
  int		_comb_arg1;
  int		_comb_arg2;
};

#endif /* !YAMS_HH_ */
