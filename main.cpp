//
// main.cpp for  in /home/fritsc_h/projets/201yams
// 
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
// 
// Started on  Mon Feb 24 18:57:23 2014 Fritsch harold
// Last update Tue Feb 25 16:32:12 2014 Fritsch harold
//

#include <iostream>
#include "Yams.hh"

int		main(int ac, char **av)
{
  if (ac == 7)
    Yams	game(av);
  return (0);
}
